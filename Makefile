
# Sub directories containing source code, except for the main programs
SUBDIRS := src src/common test/unittest

#
# Set libraries, paths, flags and options
#

#Basic flags every build needs
LIBS=-lz
CPPFLAGS=-O3 -std=c++11 -g -pthread -fPIC
CFLAGS=-O3
CXX=g++
CC=gcc

H5_LIB=./lib/libhdf5_hl.a ./lib/libhdf5.a
H5_INCLUDE=-I./include
LIBS += -ldl -lboost_unit_test_framework

# Bulild and link the libhts submodule
HTS_LIB=./htslib/libhts.a
HTS_INCLUDE=-I./htslib

# Boost library
BOOST_INCLUDE=-I./boost_1_59_0

# Include the src subdirectories
SNP_INCLUDE=$(addprefix -I./, $(SUBDIRS))

# Add include flags
CPPFLAGS += $(H5_INCLUDE) $(HTS_INCLUDE) $(SNP_INCLUDE) $(BOOST_INCLUDE)

# Main programs to build
PROGRAM=utsnp

all: $(PROGRAM)

# Download and build htslib
htslib/libhts.a:
	git clone https://github.com/samtools/htslib.git
	cd htslib; make

#
# Download and build HDF5
#
parentdir=$(shell pwd)
$(H5_LIB):
	wget https://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.14/src/hdf5-1.8.14.tar.gz
	tar -xzf hdf5-1.8.14.tar.gz
	cd hdf5-1.8.14; ./configure --enable-cxx --prefix=${parentdir}; make; make install

# Source files
CPP_SRC := $(foreach dir, $(SUBDIRS), $(wildcard $(dir)/*.cpp))
C_SRC := $(foreach dir, $(SUBDIRS), $(wildcard $(dir)/*.c))
EXE_SRC=src/main/utsnp.cpp

# Automatically generated object names
CPP_OBJ=$(CPP_SRC:.cpp=.o)
C_OBJ=$(C_SRC:.c=.o)

# Generate dependencies
PHONY=depend
depend: .depend

.depend: $(CPP_SRC) $(C_SRC) $(EXE_SRC) $(H5_LIB)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $(CPP_SRC) $(C_SRC) > ./.depend;

include .depend

# Compile objects
.cpp.o:
	$(CXX) -o $@ -c $(CPPFLAGS) -fPIC $<

.c.o:
	$(CC) -o $@ -c $(CFLAGS) -fPIC $<
# Link main executable
$(PROGRAM): src/main/utsnp.o $(CPP_OBJ) $(C_OBJ) $(HTS_LIB) $(H5_LIB)
	$(CXX) -o $@ $(CPPFLAGS) -fPIC $< $(CPP_OBJ) $(C_OBJ) $(HTS_LIB) $(H5_LIB) $(LIBS)

clean:
	rm utsnp $(CPP_OBJ) $(C_OBJ) src/main/utsnp.o
