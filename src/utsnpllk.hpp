// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Tue May 26 14:59:19 2015 (+0100)
// Filename: utsnpllk.hpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:


#ifndef UTSNPLLK_HPP
#define UTSNPLLK_HPP

#include <stdio.h>
#include <stdlib.h>
#include <htslib/vcf.h>
#include <htslib/vcfutils.h>
#include <htslib/hts.h>
#include <htslib/synced_bcf_reader.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cassert>
#include <bitset>
#include <iostream>
#include <cmath>
#include <utility>
#include "common/util.hpp"
#include "genotype.hpp"

namespace UTSNP {

/**
 * Haplotype likelihood using Li and Stephen's copying model
 */
class HapCopyHMM {
 public:
  double LogMin;
  GenomicRegion *gr;
  double theta;
  unsigned int M;  // number of sites
  unsigned int N;  // number of haplotypes
  unsigned int minac;
  unsigned int maxac;
  // llk [query][position]
  std::vector<std::vector<double> > llk;
  // [query i][position j]
  std::vector<std::vector<double> > leftnorm;
  std::vector<std::vector<double> > rightnorm;
  // probability matrices
  // [query i][donor s][position j]
  // normalised probabilities
  std::vector<std::vector<std::vector<double> > > left;
  // normalised probabilities
  std::vector<std::vector<std::vector<double> > > right;

  // struct object to hold data for one target position
  struct t_post {
    std::vector<size_t> query_idx;
    std::vector<size_t> donor_idx;
    size_t chrm;
    size_t pos;
    std::string ref;
    std::string alt;
    size_t j_target;
    // [nq][nd]
    std::vector<std::vector<double> > fw;
    std::vector<std::vector<double> > bw;
    std::vector<std::vector<double> > pst;
    // likelihoods of posteriors in copying
    double l1, l2, ln;
    t_post(void): pos(0)
                , j_target(0)
                , l1(0)
                , l2(0)
                , ln(0) {}
  };

  // data for all target sites, indexed by pos
  std::map<size_t, t_post> tmap;

  explicit HapCopyHMM(GenomicRegion *gr_):
      LogMin(-150)
      , gr(gr_)
      , M(gr->m_M)
      , N(gr->m_N) {
  }
  ~HapCopyHMM(void) {}
  void ForwardBackwardEveryone(void);
  void PositionLikelihood(void);
  void Debug(std::string outpath,
             unsigned int j,
             std::string tag);

  // populate query/donor index information in tlist
  void SetQueries();
  void SetQueries(size_t startpos, size_t endpos);
  void ComputePosterior(void);
  void WriteOutput(std::string outfilename,
                   std::string tag = "");


 private:
  double e0;  // emmission when no mutation
  double e1;  // emmission when mutation
  // posterior in log scale
  std::vector<double> r1;  // Pr(recombing)
  void SetEmmission(void);
  void SetTransition(void);
  void Forward(void);
  void Backward(void);
  void Populate_t_post(const std::vector<size_t>& qidx,
                       const std::vector<size_t>& didx,
                       std::vector<std::vector<double> >* tpmx,
                       const std::vector<std::vector<double> > &prob);
  void MutOriginTest(t_post* tp);
};

/**
 * High level method for running the analysis
 */
class HapOri {
 public:
  HapOri(void);
  ~HapOri(void);
  static int Help(void);
  int Run(int argc, char* argv[]);
 private:
  struct HapOriSettings;
  HapOriSettings *m_settings;
};

}  // namespace UTSNP

#endif
