// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Mon Jul  6 09:34:14 2015 (+0100)
// Filename: sim.hpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#ifndef SIM_HPP
#define SIM_HPP

#include <string>
#include "genotype.hpp"

namespace UTSNP {

struct scrmOptions {
  std::string cmd;
  int n;  // number of chromosomes
  double t;
  double r;
  int length;
  scrmOptions(void): n(0)
                   , t(0.)
                   , r(0.)
                   , length(0) {
  }
};

class Sim {
 public:
  std::string scrmfile;
  unsigned int len;  // length specified in -r of SCRM
  Sim(void);
  ~Sim(void);
  void Read(scrmOptions *so);
  int Run(int argc, char* argv[]);
  friend std::ostream& operator<<(std::ostream& os, const Sim& s);

 private:
  struct SimSettings;
  SimSettings *m_settings;
  GenomicRegion *gr;
  unsigned int SetQuery();
};



}  // namespace UTSNP

#endif


// sim.hpp ends here
