// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Tue Jul  7 15:44:07 2015 (+0100)
// Filename: genotype.hpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#ifndef GENOTYPE_HPP
#define GENOTYPE_HPP
#include <string>
#include <map>
#include <vector>
#include <utility>
#include "htslib/synced_bcf_reader.h"

namespace UTSNP {

const unsigned int Ne = 1.0e4;
const double rho0 = 5.0e-9;

/**
 * Store data related to a genomic section
 */
class GenomicRegion {
 public:
  unsigned int m_M; /* number of sites */
  unsigned int m_N; /* number of haplotypes */
  std::string m_chrom; /* chromosome name */
  unsigned int m_start;
  unsigned int m_end;
  unsigned int m_ploidy; /* ploidy for this region */
  // minimum allele frequency for a site to be considered
  unsigned int min_ac;
  // minimum allele frequency for untaggable SNP
  unsigned int min_t_ac;
  // maxmum allele frequency for untaggable SNP
  unsigned int max_t_ac;
  // index of donor/query haps for each site (first index)
  // e.g. exclude haps that carry the rare allele
  std::vector<int> donor_idx;
  std::vector<int> query_idx;
  // chrm, pos, ref, first alt
  std::vector<int> chrm;
  std::vector<unsigned int> pos;
  std::vector<int> majorA;  // major allele at pos
  std::vector<unsigned int> minorAC;  // minor allele count
  std::vector<std::string> ref;
  std::vector<std::string> alt1;
  std::vector<std::string> m_samples; /*list of samples*/
  // Genotype matrix, size = 2N(samples)*M(sites)
  // The [variants][samples]
  std::vector<std::vector<unsigned int> > m_matrix;
  // per base recombination rate scaled at 4N_e, length of N(pos) -1
  std::vector<double> rho;
  // genetic map data
  std::vector<std::pair<int, double> > gmap;
  // hamming_distance [queryi][donori]
  std::vector<std::vector<unsigned int> > hamd;
  GenomicRegion(std::string chrom,
               unsigned int start,
               unsigned int end,
               unsigned int ploidy = 2):
      m_chrom(chrom),
      m_start(start),
      m_end(end),
      m_ploidy(ploidy),
      min_ac(0),  // default threshold for min AC
      min_t_ac(0),  // default threshold for utsnp
      max_t_ac(0) {  // default threshold for utsnp
  }
  ~GenomicRegion() {
  }
  // print this object - for debugging
  friend std::ostream& operator<<(std::ostream& os, const GenomicRegion& gr);

  // scan region and find variants within allele frequency range
  static std::vector<int> VariantsInRange(
      const std::string vcfname,
      const std::string reg,
      const unsigned int minac,
      const unsigned int maxac) {
    std::vector<int> _pos(0);

    bcf_srs_t *sr = bcf_sr_init();
    bcf_sr_set_regions(sr, reg.c_str(), 0);
    bcf_sr_add_reader(sr, vcfname.c_str());
    bcf_hdr_t *hdr = bcf_sr_get_header(sr, 0);
    int nsmpl = bcf_hdr_nsamples(hdr);
    fprintf(stderr, "VCF file contains %i samples\n", nsmpl);
    unsigned int n = 0;
    int * gt = NULL;
    // Total number of alternate alleles in called genotypes
    const char *key = "AC";
    unsigned int *ac = NULL;
    int nac = 0;
    // actual BCF record
    bcf1_t *line = NULL;
    while ( bcf_sr_next_line(sr) ) {
      line = bcf_sr_get_line(sr, 0);
      if (!bcf_is_snp(line))
        continue;
      int nc = bcf_get_info_int32(hdr, line, key, &ac, &nac);
      if (nc < 0) {
        continue;  // no AC information
      }

      // skip if allele frequency too low
      if (*ac < minac || *ac > maxac) {
        continue;
      }
      _pos.push_back(line -> pos);
      n++;
    }
    bcf_sr_destroy(sr);
    free(gt);
    return _pos;
  }

  GenomicRegion* LoadGenomicRegon(GenomicRegion *gr,
                                  const std::string vcfname);
  GenomicRegion* LoadGenomicRegon(GenomicRegion *gr,
                                  const std::string vcfname,
                                  unsigned int _start,
                                  unsigned int _end);

  void FetchGeneticMap(std::string fileGeneticMap);
  void RandomMutate(unsigned int seed);
  void HammingDistance(void);
  void RemoveVariants(const std::vector<unsigned int> &_pos);

 private:
  double PerBaseRho(double rho);
};



}  // namespace UTSNP

#endif


// genotype.hpp ends here
