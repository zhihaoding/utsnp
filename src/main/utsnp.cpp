// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Sun May 17 15:09:31 2015 (+0100)
// Filename: utsnp.hpp
// Description:
//
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------

// Code:

#include <stdio.h>
#include <stdlib.h>
#include <htslib/vcf.h>
#include <htslib/vcfutils.h>
#include <htslib/hts.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include "utsnpllk.hpp"
#include "util.hpp"
#include "sim.hpp"

static const std::string LLK  = "likelihood";
static const std::string HELP          = "help";
static const std::string LONG_HELP     = "--help";
static const std::string SHORT_HELP    = "-h";
static const std::string VERSION       = "version";
static const std::string LONG_VERSION  = "--version";
static const std::string SHORT_VERSION = "-v";


// determine if string is a help constant
static bool IsHelp(char* str) {
    return (str == HELP ||
            str == LONG_HELP ||
            str == SHORT_HELP);
}

// determine if string is a version constant
static bool IsVersion(char* str) {
    return (str == VERSION ||
             str == LONG_VERSION ||
             str == SHORT_VERSION);
}

// print help info
int Help(int argc, char** argv) {
  // check for 'bamtools help COMMAND' to print tool-specific help message
  if (argc > 2) {
    UTSNP::HapOri * runner = new UTSNP::HapOri();
    return runner->Help();
  }

  std::cerr << std::endl;
  std::cerr << "usage: " << std::endl;
  std::cerr << std::endl;
  std::cerr << "Available commands:" << std::endl;
  std::cerr << "\tllk           compute llk" << std::endl;
  std::cerr << std::endl;
  return EXIT_SUCCESS;
}

// print version info
int Version(void) {
  std::stringstream versionStream;
  versionStream << "VERSION_MAJOR" << "."
                  << "VERSION_MINOR" << "."
                  << "VERSION_BUILD";
    std::cout << std::endl;
    std::cout << "utsnp " << versionStream.str() << std::endl;
    std::cout << "Author: " << std::endl;
    std::cout << "(c) 2015- , " << std::endl;
    std::cout << std::endl;
    return EXIT_SUCCESS;
}



int main(int argc, char* argv[]) {
  if (argc <= 1) {
    printf("error: no command provided\n");
    return 0;
  } else {
    std::string command(argv[1]);
    if (command == "help" || command == "--help") {
      Help(argc, argv);
      return 0;
    } else if (command == "llk") {
      UTSNP::HapOri* runner = new UTSNP::HapOri();
      runner -> Run(argc, argv);
      // UTSNP::hapori_main(argc - 1, argv + 1);
      delete runner;
      return 0;
    } else if (command == "sim") {
      UTSNP::Sim* runner = new UTSNP::Sim();
      runner -> Run(argc, argv);
      delete runner;
      return 0;
    }
    return 0;
  }
}

