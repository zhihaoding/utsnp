// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Mon Jun  8 13:36:38 2015 (+0100)
// Filename: util.cpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#include <string>
#include <vector>
#include <istream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <utility>
#include <cctype>
#include "util.hpp"


std::vector<std::string> split(std::string in, char delimiter) {
    std::vector<std::string> out;
    size_t lastPos = 0;
    size_t pos = in.find_first_of(delimiter);
    while (pos != std::string::npos) {
        out.push_back(in.substr(lastPos, pos - lastPos));
        lastPos = pos + 1;
        pos = in.find_first_of(delimiter, lastPos);
    }
    out.push_back(in.substr(lastPos));
    return out;
}

std::vector<std::string> splitbywhitespace(std::string in) {
    std::vector<std::string> out;
    std::string word;
    for (size_t i=0; i < in.length(); ++i) {
      if (!isspace(in[i])) {
        word += in[i];
      } else {
        if (word.length() > 0) {
          out.push_back(word);
          word = "";
        }
      }
    }
    return out;
}



std::string parentdir(std::string filepath) {
  size_t lastDirPos = filepath.find_last_of('/');
  if (lastDirPos == std::string::npos)
    return filepath;  // no directories
  else
    return filepath.substr(0, lastDirPos);
}

void tokenize(std::string str,
              std::vector<std::string>* tokens) {
  std::stringstream ss(str);
  std::string s;
  while (getline(ss, s)) {
    tokens -> push_back(s);
  }
}

void tokenize(std::string str,
              char delimiter,
              std::vector<std::string>* tokens) {
  std::stringstream ss(str);
  std::string s;
  while (getline(ss, s, delimiter)) {
      tokens -> push_back(s);
    }
}

void readBedAsRegionSting(std::string filename,
                     std::vector<std::string>* regions) {
  std::ifstream inputFile(filename);
  std::string line;
  while (getline(inputFile, line)) {
    std::istringstream ss(line);
    std::string chrm, s, e, anno;
    ss >> chrm >> s >> e >> anno;
    regions -> push_back(chrm + ":" + s + "-" + e);
    }
}



// Remove a single file extension from the filename
std::string stripExtension(const std::string& filename) {
  size_t suffixPos = filename.find_last_of('.');
  if (suffixPos == std::string::npos)
    return filename;
  else
    return filename.substr(0, suffixPos);
}


// util.cpp ends here
