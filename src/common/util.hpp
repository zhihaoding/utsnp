// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Mon Jun  8 13:07:30 2015 (+0100)
// Filename: util.hpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:


#ifndef UTIL_HPP
#define UTIL_HPP

#include <sys/stat.h>
#include <stdio.h>
#include <assert.h>
#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <functional>
#include <locale>

/******************************************
 *  String related
 ******************************************/

// trim from start
static inline std::string* ltrim(std::string *s) {
  s->erase(s->begin(),
          std::find_if(s->begin(), s->end(),
                       std::not1(std::ptr_fun<int, int>(std::isspace))));
  return s;
}

// trim from end
static inline std::string* rtrim(std::string *s) {
  s -> erase(std::find_if(s->rbegin(), s->rend(),
                       std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
          s->end());
  return s;
}

// trim from both ends
static inline std::string* trim(std::string *s) {
  return ltrim(rtrim(s));
}


// split input string by white space
std::vector<std::string> split(std::string in, char delimiter);
std::vector<std::string> splitbywhitespace(std::string in);


/******************************************
 *  IO
 ******************************************/
// return the dir where the file is in
std::string parentdir(std::string filepath);

// strip off file extension
std::string stripExtension(const std::string& filename);

// check if a file is accessible
inline bool fileExists(const std::string& filename) {
  struct stat buffer;
  return (stat (filename.c_str(), &buffer) == 0);
}

inline void removeFileIfExists(const std::string& filename) {
  if ( fileExists(filename) ) {
    if ( remove(filename.c_str()) != 0 )
      fprintf(stderr, "Error deleting file %s\n", filename.c_str());
    else
      fprintf(stderr, "Existing file %s deleted\n", filename.c_str());
  }
}


/******************************************
 *  Vector related
 ******************************************/

template<typename T>
class Vec : public std::vector<T> {
 public:
  using std::vector<T>::vector;
  T& operator[](int i) {
    return std::vector<T>::at(i);
  }
  const T& operator[](int i) const {
    return std::vector<T>::at(i);
  }
};


// sum over vectors
template <typename T>
T VectorSum(std::vector<T> *v, unsigned int start, unsigned int end) {
  assert(start >= 0 && end >= 0);
  assert(start <= end);
  assert(start < (*v).size() && end <= (*v).size());
  T sum = 0;
  for (unsigned int i = start; i < end; i++) {
      sum += (*v)[i];
  }
  return sum;
}

// multiply over vectors
template <typename T>
T VectorMultiply(std::vector<T> *v, unsigned int start, unsigned int end) {
  assert(start >= 0 && end >= 0);
  assert(start <= end);
  assert(start < (*v).size() && end <= (*v).size());
  T mul = 0;
  for (unsigned int i = start; i < end; i++) {
      mul *= (*v)[i];
  }
  return mul;
}

// initialize vectors
template <typename T>
void InitializeMatrix(std::vector<std::vector<std::vector<T> > >* m3,
                      int d1, int d2, int d3) {
  (*m3).clear();
  (*m3).resize(d1);
  for (int i = 0; i < d1; ++i) {
    (*m3)[i].resize(d2);
    for (int j = 0; j < d2; ++j)
      (*m3)[i][j].resize(d3);
  }
}

template <typename T>
void InitializeMatrix(std::vector<std::vector<T> >* m2,
    int d1, int d2) {
  (*m2).clear();
  (*m2).resize(d1);
  for (int i = 0; i < d1; ++i) {
    (*m2)[i].resize(d2);
  }
}

void readBedAsRegionSting(std::string filename,
                          std::vector<std::string>* regions);

template <typename T>
inline void uniqSortedVector(std::vector<T> *v) {
  std::sort((*v).begin(), (*v).end());
  typedef typename std::vector<T>::iterator iterator;
  iterator it;
  it = std::unique((*v).begin(), (*v).end());
  (*v).resize(std::distance((*v).begin(), it));
}


#endif


// util.hpp ends here
