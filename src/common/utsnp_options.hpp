// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Tue May 26 14:46:58 2015 (+0100)
// Filename: utsnp_options.hpp
// Description:
//
// ***************************************************************************
// bamtools_options.h (c) 2010 Derek Barnett, Erik Garrison
// Marth Lab, Department of Biology, Boston College
// ---------------------------------------------------------------------------
// Parses command line arguments and creates a help menu
// ---------------------------------------------------------------------------
// Modified from:
// The Bamtools command line parser class: bamtools_options
// ***************************************************************************
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#ifndef UTSNP_OPTIONS_HPP
#define UTSNP_OPTIONS_HPP

#include <map>
#include <string>
#include <vector>
#include <typeinfo>

#define ARGUMENT_LENGTH       35
#define DESC_LENGTH_FIRST_ROW 30
#define DESC_LENGTH           42
#define MAX_LINE_LENGTH       78


#ifdef WIN32
  #define snprintf _snprintf
  typedef __int64          int64_t;
  typedef unsigned __int64 uint64_t;
  #define strtoui64 _strtoui64
#else
  #define strtoui64 strtoull
#endif

class Variant {
 public:
  Variant(void) : data(NULL) { }
  Variant(const Variant& other) { 
     if ( other.data != NULL ) 
         other.data->AddRef();
     data = other.data;
  }

  ~Variant(void) { 
     if ( data != NULL ) 
         data->Release();
  }

  // NOTE: This code takes care of self-assignment.
  // DO NOT CHANGE THE ORDER of the statements.
  Variant& operator= (const Variant& rhs) {
     if ( rhs.data != NULL ) 
         rhs.data->AddRef();
     if ( data != NULL ) 
         data->Release();
     data = rhs.data;
     return *this;
  }

  // This member template constructor allows you to
  // instance a variant_t object with a value of any type.
  template<typename T>
  Variant(T v) 
     : data(new Impl<T>(v)) 
  { 
     data->AddRef(); 
  }

  // This generic conversion operator let you retrieve
  // the value held. To avoid template specialization conflicts,
  // it returns an instance of type T, which will be a COPY
  // of the value contained.
  template<typename T> 
  operator T() const { 
     return CastFromBase<T>(data)->data;
  }

  // This forms returns a REFERENCE and not a COPY, which
  // will be significant in some cases.
  template<typename T> 
  const T& get(void) const { 
     return CastFromBase<T>(data)->data; 
  }

  template<typename T> 
  bool is_type(void) const { 
     return typeid(*data)==typeid(Impl<T>); 
  }

  template<typename T> 
  bool is_type(T v) const { 
     return typeid(*data)==typeid(v); 
  }

  private:
  struct ImplBase {

     ImplBase() : refs(0) { }
     virtual ~ImplBase(void) { }

     void AddRef(void) { ++refs; }
     void Release(void) { 
         --refs;
         if ( refs == 0 ) delete this;
     }

     size_t refs;
  };

  template<typename T>
  struct Impl : ImplBase {
     Impl(T v) : data(v) { }
     ~Impl(void) { }
     T data;
  };

  // The following method is static because it doesn't
  // operate on variant_t instances.
  template<typename T> 
  static Impl<T>* CastFromBase(ImplBase* v) {
     // This upcast will fail if T is other than the T used
     // with the constructor of variant_t.
     Impl<T>* p = dynamic_cast< Impl<T>* > (v);
     if ( p == NULL ) 
         throw std::invalid_argument( typeid(T).name() + std::string(" is not a valid type") );
     return p;
  }

  ImplBase* data;
};


struct Option {
    // data members
    std::string Argument;
    std::string ValueDescription;
    std::string Description;
    bool StoreValue;
    bool HasDefaultValue;
    Variant DefaultValue;

    // constructor
    Option(void)
        : StoreValue(true)
        , HasDefaultValue(false)
    { }
};

struct OptionValue {
  
    // data members
    bool* pFoundArgument;
    void* pValue;
    std::string ValueTypeDescription;
    bool UseVector;
    bool StoreValue;
    bool IsRequired;
    Variant VariantValue;

    // constructor
    OptionValue(void)
        : pFoundArgument(NULL)
        , pValue(NULL)
        , UseVector(false)
        , StoreValue(true)
        , IsRequired(false)
  {} 
};

struct OptionGroup {
    std::string Name;
    std::vector<Option> Options;
};

class Options {
    // add option/argument rules
    public:
        // adds a simple option to the parser
        static void AddOption(const std::string& argument, 
                       const std::string& optionDescription, 
                       bool& foundArgument, 
                       OptionGroup* group);
                       
        // adds a value option to the parser
        template<typename T>
        static void AddValueOption(const std::string& argument, 
                            const std::string& valueDescription, 
                            const std::string& optionDescription, 
                            const std::string& valueTypeDescription, 
                            bool& foundArgument, 
                            T& val, 
                            OptionGroup* group);
                            
        // adds a value option to the parser (with a default value)
        template<typename T, typename D>
        static void AddValueOption(const std::string& argument, 
                            const std::string& valueDescription, 
                            const std::string& optionDescription, 
                            const std::string& valueTypeDescription, 
                            bool& foundArgument, 
                            T& val, 
                            OptionGroup* group, 
                            D& defaultValue);
       
    // other API methods
    public:
        // creates an option group
        static OptionGroup* CreateOptionGroup(const std::string& groupName);    
        // displays the help menu
        static void DisplayHelp(void);
        // parses the command line
        static void Parse(int argc, char* argv[], int offset = 0);
        // sets the program info
        static void SetProgramInfo(const std::string& programName,
                                   const std::string& description,
                                   const std::string& arguments);
        // returns string representation of stdin
        static const std::string& StandardIn(void);
        // returns string representation of stdout
        static const std::string& StandardOut(void);
        
    // static data members
    private:
        // the program name
        static std::string m_programName;
        // the main description
        static std::string m_description;
        // the example arguments
        static std::string m_exampleArguments;
        // stores the option groups
        static std::vector<OptionGroup> m_optionGroups;
        // stores the options in a map
        static std::map<std::string, OptionValue> m_optionsMap;
        // string representation of stdin
        static const std::string m_stdin;
        // string representation of stdout
        static const std::string m_stdout;
};

// adds a value option to the parser
template<typename T>
void Options::AddValueOption(const std::string& argument, 
                             const std::string& valueDescription, 
                             const std::string& optionDescription, 
                             const std::string& valueTypeDescription, 
                             bool& foundArgument, 
                             T& val, 
                             OptionGroup* group) 
{
        Option o;
        o.Argument         = argument;
        o.ValueDescription = valueDescription;
        o.Description      = optionDescription;
        group->Options.push_back(o);

        OptionValue ov;
        ov.pFoundArgument       = &foundArgument;
        ov.pValue               = (void*)&val;
        ov.VariantValue         = val;
        ov.IsRequired           = (valueTypeDescription.empty() ? false : true);
        ov.ValueTypeDescription = valueTypeDescription;
        m_optionsMap[argument] = ov;
}

// adds a value option to the parser (with a default value)
template<typename T, typename D>
void Options::AddValueOption(const std::string& argument, 
                             const std::string& valueDescription, 
                             const std::string& optionDescription, 
                             const std::string& valueTypeDescription, 
                             bool& foundArgument, 
                             T& val, 
                             OptionGroup* group, 
                             D& defaultValue) 
{
        Option o;
        o.Argument         = argument;
        o.ValueDescription = valueDescription;
        o.Description      = optionDescription;
        o.DefaultValue     = defaultValue;
        o.HasDefaultValue  = true;
        group->Options.push_back(o);

        OptionValue ov;
        ov.pFoundArgument       = &foundArgument;
        ov.pValue               = (void*)&val;
        ov.VariantValue         = val;
        ov.IsRequired           = (valueTypeDescription.empty() ? false : true);
        ov.ValueTypeDescription = valueTypeDescription;
        m_optionsMap[argument] = ov;
}


#endif


// utsnp_options.hpp ends here
