// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Mon Jul  6 09:34:42 2015 (+0100)
// Filename: sim.cpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <regex>
#include "sim.hpp"
#include "common/util.hpp"
#include "genotype.hpp"
#include "utsnpllk.hpp"
#include "common/utsnp_options.hpp"


namespace UTSNP {

/********************************************
 * Implementation for Simulation methods
 ********************************************/

struct Sim::SimSettings {
  std::string ScrmResultFile;
  std::string OutputFilename;
  std::string tag;
  bool HasInputFile;
  bool HasOutputFile;
  bool SetPermNumber;
  bool SetMinac;
  bool SetMaxac;
  bool Settheta;
  bool Setrho;
  bool Settag;
  unsigned int nPerm;
  unsigned int Minac;
  unsigned int Maxac;
  double theta;
  double rho;

  // constructor
  SimSettings(void)
      : ScrmResultFile("")
      , tag("")
      , HasInputFile(false)
      , HasOutputFile(false)
      , SetPermNumber(false)
      , SetMinac(false)
      , SetMaxac(false)
      , Settheta(false)
      , Setrho(false)
      , Settag(false)
      , nPerm(0)
      , Minac(0)
      , Maxac(0)
      , theta(0)
      , rho(0) {
  }
};

Sim::Sim(void): m_settings(new SimSettings)
              , gr(new GenomicRegion("sim", 0, 0, 1)) {
  // set program details
  Options::SetProgramInfo("utsnp sim",
                          "scan simulated results",
                          "[-in <filename>] [-out <filename>]");
  // set up options
  OptionGroup* IO_Opts = Options::CreateOptionGroup("Input & Output");
  Options::AddValueOption("-in",
                          "scrmfile",
                          "the input file written by SCRM", "",
                          m_settings->HasInputFile,
                          m_settings->ScrmResultFile,
                          IO_Opts, Options::StandardIn());
  Options::AddValueOption("-out",
                          "outfile",
                          "the output file", "",
                          m_settings->HasOutputFile,
                          m_settings->OutputFilename,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-nperm", "permutation",
                          "number of permutations",    "",
                          m_settings->SetPermNumber,
                          m_settings->nPerm,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-tag", "tag",
                          "tag to write in output",    "",
                          m_settings->Settag,
                          m_settings->tag,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-minac", "minac",
                          "lower bound of allele count for target sites",    "",
                          m_settings->SetMinac,
                          m_settings->Minac,
                          IO_Opts,
                          Options::StandardOut());

  Options::AddValueOption("-maxac", "maxac",
                          "upper bound of allele count for target sites",    "",
                          m_settings->SetMaxac,
                          m_settings->Maxac,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-r", "rho",
                          "recombination events",    "",
                          m_settings->Setrho,
                          m_settings->rho,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-t", "theta",
                          "mutation events",    "",
                          m_settings->Settheta,
                          m_settings->theta,
                          IO_Opts,
                          Options::StandardOut());
}

Sim::~Sim(void) {
  delete m_settings;
  delete gr;
  m_settings = 0;
}


void Sim::Read(scrmOptions *so) {
  std::string line;
  std::ifstream inputFile(m_settings->ScrmResultFile);
  int i = -1;
  int i_gt = 0;
  std::string cmd;
  std::vector<std::string> cmditems;
  std::string seed;
  unsigned int npos;
  std::vector<std::string> posstr;
  std::vector<std::vector<unsigned int> > gtmatrix;
  bool takingGT = false;

  while (getline(inputFile, line)) {
    ++i;
    if (i == 0) {
      rtrim(&line);
      cmd = line;
      // scrm 5000 1 -t 200 -r 200 1000000 -O -seed 2962091034 -p 14
      cmditems = splitbywhitespace(cmd);
      fprintf(stderr, "scrm command: %s\n", cmd.c_str());
      so->cmd = cmd;
      so->n = std::stoi(cmditems[1]);
      so->t = std::stoi(cmditems[4]);
      so->r = std::stoi(cmditems[6]);
      so->length = std::stoi(cmditems[7]);
      fprintf(stderr, "loading scrm simulation output\n");
      fprintf(stderr, "  number of haps: %d\n", so->n);
      fprintf(stderr, "  mutations(-t): %e\n", so->t);
      fprintf(stderr, "  recombinations(-r): %e\n", so->r);
      fprintf(stderr, "  segment length: %d\n", so->length);
    } else if (i == 1) {
      std::istringstream ss(line);
      ss >> seed;
      fprintf(stderr, "  seed: %s\n", seed.c_str());
    } else if ( i > 3 && !takingGT ) {
      std::size_t found = line.find("positions");
      if (found != std::string::npos) {
        rtrim(&line);
        posstr = split(line, ' ');
        posstr.erase(posstr.begin());
        npos = posstr.size();
        takingGT = true;
        continue;
      }
    }
    if (takingGT) {
      std::vector<unsigned int> hap(npos, 0);
      rtrim(&line);
      std::string s(line);
      for (size_t j = 0 ; j < s.length(); j++) {
        hap[j] = s[j] - '0';  // convert char to int
      }
      gtmatrix.push_back(hap);
      ++i_gt;
    }
  }

  gr->m_N = gtmatrix.size();
  gr->m_M = npos;

  assert(gr->m_N == static_cast<unsigned int>(so->n));
  printf("number of sites: %d\n", npos);
  for (size_t i = 0; i < posstr.size(); i++) {
    double p = std::stof(posstr[i]) * so->length;
    gr->pos.push_back(static_cast<int>(p));
  }
  printf("\n");
  InitializeMatrix(&gr->m_matrix, gr->m_M, gr->m_N);
  printf("Initialized genotype matrix\n");

  // assign genotype information to gr object
  for (size_t j = 0; j < npos; j++) {
    unsigned int ac = 0;
    std::vector<int> imuthap;
    for (size_t i = 0; i < gr->m_N; i++) {
      if (gtmatrix[i][j] == 1) {
        ac += 1;
        imuthap.push_back(i);
      }
      gr->m_matrix[j][i] = gtmatrix[i][j];
    }
    gr->chrm.push_back(0);
    gr->ref.push_back("0");
    gr->alt1.push_back("1");
    gr->minorAC.push_back(ac);
    gr->majorA.push_back(0);
  }
  // build set to genome region
  for (unsigned int i = 0; i < gr->m_N; i++) {
    gr->m_samples.push_back(std::to_string(i));
  }
}

int Sim::Run(int argc, char* argv[]) {
  // parse command line arguments
  Options::Parse(argc, argv, 1);

  // check IO settings
  if (!fileExists(m_settings->ScrmResultFile)) {
    printf("Error opening input file %s \n",
           m_settings->ScrmResultFile.c_str());
    printf("Please check if the input SCRM result file is accessible \n");
    exit(EXIT_FAILURE);
  }
  std::cout << "SCRM result file:" << m_settings->ScrmResultFile << '\n';
  std::cout << "program output file:" << m_settings->OutputFilename << '\n';
  std::string outpath = parentdir(m_settings->OutputFilename);
  printf("output directory %s\n", outpath.c_str());

  gr->min_ac = m_settings->Minac;  // below this it's too rare, will be excluded
  gr->max_t_ac  = m_settings->Maxac;

  printf("allele frequency range [%d,%d)\n", gr->min_ac, gr->max_t_ac);
  if (gr->min_ac == 0 || gr->max_t_ac == 0) {
    fprintf(stderr, "allele frequency range not set!\n");
    exit(EXIT_FAILURE);
  }

  // Load SCRM output
  scrmOptions sc;
  Read(&sc);
  gr->m_end = sc.length;

  // Set theta and rho
  double theta, rhoi, len(sc.length);
  if (m_settings->theta == 0 &&
      m_settings->rho == 0 ) {  // use numbers in simulation result
    theta = sc.t;
    rhoi = sc.r;
    printf("Set theta:%e, rho:%e using information in scrm output\n",
           theta, rhoi);
  } else {
    theta = m_settings->theta;
    rhoi = m_settings->rho;
    printf("Set theta:%e, rho:%e as specified\n", theta, rhoi);
  }

  gr->rho.resize(gr->m_M-1);
  for (size_t i = 0; i < gr->rho.size(); i++) {
    gr->rho[i] = rhoi/len;  // set same rho to all segments
  }

  // Find targets
  HapCopyHMM* hcopy = new HapCopyHMM(gr);
  hcopy->SetQueries();
  fprintf(stderr, "found %lu target sites in region\n", hcopy->tmap.size());

  // Do HMM scan
  std::cout << "Compute likelihood" << '\n';
  hcopy->theta = theta;
  hcopy->ForwardBackwardEveryone();
  hcopy->ComputePosterior();
  hcopy->WriteOutput(m_settings->OutputFilename,
                     m_settings->tag);
  delete hcopy;

  return 0;
}

}  // namespace UTSNP

// sim.cpp ends here
