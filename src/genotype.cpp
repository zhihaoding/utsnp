// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Tue Jul  7 15:44:25 2015 (+0100)
// Filename: genotype.cpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <sstream>
#include "genotype.hpp"
#include "utsnpllk.hpp"
#include "common/utsnp_options.hpp"
#include "common/util.hpp"
#include "hdf5.h"
#include "genotype.hpp"

namespace UTSNP {

/********************************************
 * Implementation for GenomicRegion
 ********************************************/


/**
 * This implementation uses htslib/synced_bcf_reader.h
 * We could constrain to a specific region. 
 */
GenomicRegion* GenomicRegion::LoadGenomicRegon(GenomicRegion *gr,
                                             const std::string vcfname) {
  return LoadGenomicRegon(gr, vcfname, gr->m_start, gr->m_end);
}

GenomicRegion* GenomicRegion::LoadGenomicRegon(GenomicRegion *gr,
                                const std::string vcfname,
                                unsigned int _start,
                                unsigned int _end) {
  bcf_srs_t *sr = bcf_sr_init();
  std::stringstream sstm;
  sstm << gr -> m_chrom << ":" << _start << "-" << _end;
  const char *regions = sstm.str().c_str();
  bcf_sr_set_regions(sr, regions, 0);
  bcf_sr_add_reader(sr, vcfname.c_str());

  bcf_hdr_t *hdr = bcf_sr_get_header(sr, 0);
  int nsmpl = bcf_hdr_nsamples(hdr);
  fprintf(stderr, "File contains %i samples\n", nsmpl);
  std::string hpi[] = {"0", "1"};
  // diploid, m_N corresponds to the number of haplotypes
  gr -> m_N = nsmpl * gr->m_ploidy;
  char** arr = hdr -> samples;
  for (int s=0; s < nsmpl; s++) {
    std::string str(arr[s]);
    for (size_t h=0; h < 2; h++) {
      gr -> m_samples.push_back(str + "_" + hpi[h]);
    }
  }

  // gr->m_samples = std::vector<std::string> (arr, arr+nsmpl);
  // create a genotype matrix, the first index is the number
  // of haplotypes
  unsigned int n = 0;
  int * gt = NULL;
  int ngt_arr = 0;
  // Total number of alternate alleles in called genotypes
  const char *key = "AC";
  unsigned int *ac = NULL;
  int nac = 0;
  int nRare = 0;

  // actual BCF record
  bcf1_t *line = NULL;
  while ( bcf_sr_next_line(sr) ) {
    line = bcf_sr_get_line(sr, 0);

    if (!bcf_is_snp(line))
      continue;
    int nc = bcf_get_info_int32(hdr, line, key, &ac, &nac);
    if (nc < 0) {
      continue;  // no AC information
    }

    //  minor allele count
    bool isAltMinor = (*ac < gr -> m_N - *ac)? true:false;
    if (isAltMinor) {
      gr->majorA.push_back(0);
    } else {
      gr->majorA.push_back(1);
    }
    unsigned int minor_allele_count = isAltMinor? *ac : gr -> m_N - *ac;
    // skip if allele frequency too low
    if (minor_allele_count < min_ac) {
      nRare++;
      continue;
    }

    char **allele  = line -> d.allele;
    gr->chrm.push_back(line -> rid);
    gr->pos.push_back(line -> pos);
    gr->ref.push_back(allele[0]);
    gr->alt1.push_back(allele[1]);
    gr->minorAC.push_back(minor_allele_count);
    // for each variant, create a vector with length equals to number
    // of haplotypes
    gr-> m_matrix.push_back(std::vector<unsigned int> (gr->m_N));
    int ngts = bcf_get_format_int32(hdr, line, "GT", &gt, &ngt_arr);
    ngts /= nsmpl;  // ngts equals ploidy
    for (int i = 0; i < nsmpl; i++) {
      for (int j = 0; j < ngts; j++) {
        // populate value into genotype matrix
        int idx = i*ngts+j;
        int allele = bcf_gt_allele(gt[idx]);
        gr -> m_matrix[n][idx] = allele;
      }
    }
    ++n;
  }

  gr -> m_M = n;
  bcf_sr_destroy(sr);
  free(gt);

  std::cout << "Loaded "<< gr -> m_M <<" variants for " <<
      gr -> m_N << " haplotypes." << std::endl;
  std::cout << "" << nRare
            << " sites were excluded due to allele count below "
            << min_ac << '\n';
  std::cout << "allele range for query: (" << gr -> min_ac
            << "-" << gr -> max_t_ac << ")" << std::endl;

  return gr;
}

void GenomicRegion::RemoveVariants(const std::vector<unsigned int> &_pos) {
  for (size_t i = pos.size()-1; i != 0; i--) {  // delete in reverse order
    for (size_t j = _pos.size() -1; j != 0; j--) {
      if (pos[i] == _pos[j]) {
          chrm.erase(chrm.begin() + i);
          pos.erase(pos.begin() + i);
          majorA.erase(majorA.begin() + i);
          ref.erase(ref.begin() + i);
          alt1.erase(alt1.begin() + i);
          m_matrix.erase(m_matrix.begin() + i);
        break;
      }
    }
  }
}

void RandomQueryDonorSplit(int ntotal,
                           int nquery,
                           int seed,
                           std::vector<int> * queryidx,
                           std::vector<int> * donoridx) {
  assert(nquery > 0 && nquery < ntotal);
  std::vector <int> samplingIndex;
  for (int i = 0; i < ntotal; ++i) {
    samplingIndex.push_back(i);
  }
  std::srand(seed);
  std::random_shuffle(samplingIndex.begin(), samplingIndex.end());
  for (int i = 0; i < ntotal; i++) {
    if (i < nquery) {
      (*queryidx).push_back(samplingIndex[i]);
    } else {
      (*donoridx).push_back(samplingIndex[i]);
    }
  }
}

/**
 * Mutate query genotypes and randomise
 * query and donor set
 */
void GenomicRegion::RandomMutate(unsigned int seed) {
  // t_pos_idx
  // find query snps
  // reset variant on query haps
  // random place same amount of mutations at pos
  // update query haps

  int nquery = query_idx.size();

  query_idx.clear();
  donor_idx.clear();
  // random selected hap index
  RandomQueryDonorSplit(m_N, nquery, seed, &query_idx, &donor_idx);
  // reset genotypes - to be implemented
}

void GenomicRegion::HammingDistance(void) {
  size_t j, t, u, nd = donor_idx.size(), nq = query_idx.size();
  assert(nd > 0 && nq > 0);
  InitializeMatrix(&hamd, nq, nd);
  for (t = 0; t < nq; t++) {
    for (u = 0; u < nd; u++) {
      hamd[t][u] = 0;
      for (j = 0; j < m_M; j++) {
        if (m_matrix[j][query_idx[t]] != m_matrix[j][donor_idx[u]])
          ++hamd[t][u];
      }
    }
  }
}

/**
 * Convert rate in genetic map (cM/Mb) to per base
 * recombination frequency at 4Ne
 */
double GenomicRegion::PerBaseRho(double rho) {
  return rho*4*Ne/(1000000*100);
}

/**
 * Use a genetic map to set recombination rate for each
 * interval between fetched variants
 */
void GenomicRegion::FetchGeneticMap(std::string fileGeneticMap) {
  assert(pos.size() > 1);  // at least 2 variants fetched
  assert(fileExists(fileGeneticMap));
  int nsnp = pos.size();
  rho = std::vector<double>(nsnp-1, 0);  // number of intervals
  std::ifstream inputFile(fileGeneticMap);
  std::string line;
  bool first = true;  // skip header line
  int pre = 0;
  int s = 0;  // last index for the snp pos
  std::cout << "Loading genetic map " << fileGeneticMap << '\n';
  std::vector<std::pair<int, int> > intervals;
  std::vector<std::vector<int> > interval_snps;
  std::vector<double> rates;
  while (getline(inputFile, line)) {
    if (first) {  // skip header line
      first = false;
      continue;
    }
    std::istringstream ss(line);
    int p;  // physical position
    double r, m;  //  combined rate(cM/Mb); genetic_map(cM)
    ss >> p >> r >> m;
    // if the rate is 0 in the genetic map, use a default value
    r = (r == 0)?rho0:PerBaseRho(r);
    if (p < pre) {
      std::cerr << "Pos in unsorted order at " << p <<
          ", whereas the previous position is " << pre << std::endl;
      exit(1);
    }
    if (p < pos[s]) {
      continue;
    }
    intervals.push_back(std::pair<int, int> (pre, p));
    std::vector<int> lsnps = std::vector<int>();
    while (s < nsnp && pos[s] >= pre && pos[s] < p) {
      lsnps.push_back(s);
      s++;
    }
    interval_snps.push_back(lsnps);
    rates.push_back(r);
    pre = p;
    if (p > pos[nsnp-1]) {
      break;
    }
  }
  std::cout << "Loaded " << interval_snps.size()
            << " genetic map regions" << '\n';
  for (size_t i = 0; i < interval_snps.size(); i++) {
    int itvstart = intervals[i].first;
    int itvend = intervals[i].second;
    double r = rates[i];
    int ns = interval_snps[i].size();
    // std::cout << "interval " << i << "(" <<
    //     intervals[i].first << "-" <<
    //     intervals[i].second << ") rate " <<
    //     rates[i] << ":";
    for (int j = 0; j < ns; j++) {
      // this is the first variant, and not the first interval
      if (j == 0 && i > 0) {
        rho[interval_snps[i-1][interval_snps[i-1].size()-1]] +=
            r*(pos[interval_snps[i][j]]-itvstart);
      }
      // std::cout << "," << interval_snps[i][j] << "("
      //           << pos[interval_snps[i][j]] << ")";
      if (j == ns -1) {  // if this is the last variant in this interval
        if (interval_snps[i][j] < nsnp-1) {  // this is not the last snp
          rho[interval_snps[i][j]] += r*(itvend-pos[interval_snps[i][j]]);
        }
      } else {
        rho[interval_snps[i][j]] =
          r*(pos[interval_snps[i][j+1]] - pos[interval_snps[i][j]]);
      }
    }
    // std::cout << std::endl;
  }

  // for (size_t j=0; j < rho.size(); j++) {
  //   std::cout << "," << rho[j];
  // }
  // std::cout << std::endl;
}



/**
 * Print some of the content of a GenomicRegion object, used for
 * debugging. 
 */
std::ostream& operator<<(std::ostream& os, const GenomicRegion& gr) {
  os << "Number of sites:" << gr.m_M << std::endl;
  os << "Number of haplotypes:" << gr.m_N << std::endl;
  os << "Ploidy:" << gr.m_ploidy << std::endl;
  os << "Number of samples:" << gr.m_N/gr.m_ploidy << std::endl;
  if (gr.m_chrom.length() > 1) {
     os << "region:" << gr.m_chrom << ":" << gr.m_start
     << "-" << gr.m_end << std::endl;
  }
  unsigned int firstNsamples = 20;  // just to check if they look right
  os << "sample names (example, first" <<
      firstNsamples << " only):" << std::endl;

  for (size_t i=0; i < gr.m_N; i++) {
    os << gr.m_samples[i] << ",";
    if (i > firstNsamples) {
      break;
    }
  }
  os << std::endl;
  os << "Genotypes of the first variant across samples:" << std::endl;
  for (size_t s=0; s < gr.m_N/gr.m_ploidy; s++) {
    os << gr.m_matrix[0][s*gr.m_ploidy];
    if (s % gr.m_ploidy == 0) {
      os << ",";
    }
  }
  os << std::endl;
  return os;
}

}  // namespace UTSNP

// genotype.cpp ends here
