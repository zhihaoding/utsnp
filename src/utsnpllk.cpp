// Copyright (C) <2015-> [Zhihao Ding]
// Author: Zhihao Ding
// Created: Tue May 26 14:59:12 2015 (+0100)
// Filename: utsnpllk.cpp
// Description:
// 
// ------------------------------------------------------------------
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------
// 
// 

// Code:

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <memory>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <sstream>
#include "genotype.hpp"
#include "utsnpllk.hpp"
#include "common/utsnp_options.hpp"
#include "common/util.hpp"

namespace UTSNP {

/********************************************
 * Implementation for HapOri
 ********************************************/

// HapOriSettings Implementation
struct HapOri::HapOriSettings {
  bool HasInputFile;
  bool HasOutputFile;
  bool HasGeneticMapFile;
  bool SetRegion;
  bool SetMinac;
  bool SetMaxac;
  bool Settheta;
  bool Setrho;
  bool Settag;
  bool Setwsize;
  std::string InputVCFFilename;
  std::string OutputFilename;
  std::string GeneticMap;
  std::string Region;
  std::string tag;
  unsigned int MinSNPs;  // minimum amount of SNPs we need to do llk calc
  unsigned int Minac;
  unsigned int Maxac;
  double theta;
  double rho;
  unsigned int wsize;

  // constructor
  HapOriSettings(void)
        : HasInputFile(false)
        , HasOutputFile(false)
        , HasGeneticMapFile(false)
        , SetRegion(false)
        , Settheta(false)
        , Setrho(false)
        , Settag(false)
        , Setwsize(false)
        , InputVCFFilename("")
        , OutputFilename("")
        , GeneticMap("")
        , Region("")
        , tag("")
        , MinSNPs(0)
        , Minac(0)
        , Maxac(0)
        , theta(0)
        , rho(0)
        , wsize(0)
  { }
};

// HapOri Implementation
int HapOri::Help(void) {
  Options::DisplayHelp();
  return 0;
}

HapOri::~HapOri(void) {
    delete m_settings;
    m_settings = 0;
}

HapOri::HapOri(void)
    : m_settings(new HapOriSettings) {
  // here we add information to define a parser
  // set program details
  Options::SetProgramInfo("utsnp llk",
                          "scan a VCF",
                          "[-in <filename>] [-out <filename>]");
  // set up options
  OptionGroup* IO_Opts = Options::CreateOptionGroup("Input & Output");
  Options::AddValueOption("-in",  "VCF filename", "the input VCF file", "",
                          m_settings->HasInputFile,
                          m_settings->InputVCFFilename,
                          IO_Opts, Options::StandardIn());
  Options::AddValueOption("-out", "filename",
                          "the output file",    "",
                          m_settings->HasOutputFile,
                          m_settings->OutputFilename,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-map",  "Genetic Map", "genetic map", "",
                          m_settings->HasGeneticMapFile,
                          m_settings->GeneticMap,
                          IO_Opts, Options::StandardIn());
  Options::AddValueOption("-g",  "Genomic region", "genomic region", "",
                          m_settings->SetRegion,
                          m_settings->Region,
                          IO_Opts, Options::StandardIn());
  Options::AddValueOption("-minac", "minac",
                          "lower bound of allele count for target sites",    "",
                          m_settings->SetMinac,
                          m_settings->Minac,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-maxac", "maxac",
                          "upper bound of allele count for target sites",    "",
                          m_settings->SetMaxac,
                          m_settings->Maxac,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-r", "rho",
                          "recombination events",    "",
                          m_settings->Setrho,
                          m_settings->rho,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-t", "theta",
                          "mutation events",    "",
                          m_settings->Settheta,
                          m_settings->theta,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-tag", "tag",
                          "tag to write in output",    "",
                          m_settings->Settag,
                          m_settings->tag,
                          IO_Opts,
                          Options::StandardOut());
  Options::AddValueOption("-w", "wsize",
                          "window size",    "",
                          m_settings->Setwsize,
                          m_settings->wsize,
                          IO_Opts,
                          Options::StandardOut());
}

void TokenizeRegion(std::string region,
                    std::string* chrm,
                    unsigned int * start,
                    unsigned int * end) {
  std::vector<std::string> first;
  first = split(region, ':');
  *chrm = first[0];
  std::vector<std::string> second;
  second = split(first[1], '-');
  *start = std::stoi(second[0]);
  *end = std::stoi(second[1]);
}

/**
 * Main Entry for this analysis
 */
int HapOri::Run(int argc, char* argv[]) {
  // parse command line arguments
  Options::Parse(argc, argv, 1);
  // VCF file for all variants (common and rare)
  if (!fileExists(m_settings -> InputVCFFilename)) {
    printf("Error opening file %s \n",
           m_settings -> InputVCFFilename.c_str());
    printf("Please check if the input VCF accessible \n");
    exit(EXIT_FAILURE);
  }
  if (!fileExists(m_settings -> GeneticMap)) {
    printf("Error opening file %s \n",
           m_settings -> GeneticMap.c_str());
    printf("Please check if the genetic map file accessible \n");
    exit(EXIT_FAILURE);
  }
  std::cout << "InputVCF:" << m_settings -> InputVCFFilename << '\n';
  std::cout << "OutputFile:" << m_settings -> OutputFilename << '\n';
  std::cout << "Genetic Map:" << m_settings -> GeneticMap << '\n';
  std::cout << "Genomic Region:" << m_settings -> Region << '\n';
  std::string chrm;
  unsigned int start, end;
  TokenizeRegion(m_settings -> Region, &chrm, &start, &end);
  std::cout << "chrm:" << chrm << ":" << start << "-" << end << '\n';

  std::ofstream out;
  std::string outfile = m_settings->OutputFilename + ".llkratio";
  removeFileIfExists(outfile);

  size_t wstart, wend;
  if (start > m_settings->wsize) {
    wstart = start - m_settings->wsize;
  } else {
    wstart = 0;
  }
  wend = end + m_settings->wsize;
  GenomicRegion* gr = new GenomicRegion(chrm, wstart, wend, 2);
  gr->min_ac = m_settings->Minac;
  gr->min_t_ac  = m_settings->Minac;
  gr->max_t_ac  = m_settings->Maxac;
  gr->LoadGenomicRegon(gr, m_settings -> InputVCFFilename);
  gr->FetchGeneticMap(m_settings -> GeneticMap);  // load rmb map

  // Find targets
  HapCopyHMM* hcopy = new HapCopyHMM(gr);
  hcopy->SetQueries(start, end);
  fprintf(stderr, "found %lu target sites in region\n", hcopy->tmap.size());

  // Do HMM scan
  std::cout << "Compute likelihood" << '\n';
  hcopy->theta = m_settings->theta;
  hcopy->ForwardBackwardEveryone();
  hcopy->ComputePosterior();
  hcopy->WriteOutput(m_settings->OutputFilename,
                     m_settings->tag);
  delete gr;
  delete hcopy;

  printf("Done.\n");

  return 0;
}


/********************************************
 * Implementation for HapCopyHMM
 ********************************************/

void HapCopyHMM::ForwardBackwardEveryone(void) {
  std::cout << "\n\n**************************************\n";
  std::cout << "*Forward and Backward algorithm \n";
  std::cout << "**************************************\n\n";
  SetEmmission();
  SetTransition();
  printf(" emmission probability e0:%g e1:%g\n", e0, e1);
  printf(" recomb probability\n");
  printf(" recombination probability Pr(recomb):%g Pr(non):%g\n",
         r1[0], 1-r1[0]);
  printf("\n");
  // set target_j to the last pos to get full forward
  std::cout << " Go foward" << '\n';
  Forward();
  std::cout << " Go backward" << '\n';
  Backward();
}

void HapCopyHMM::SetEmmission(void) {
  double ndd(gr->m_N);
  e1 = 0.5 * theta/(ndd+theta);  // mutation
  e0 = 1-e1;  // no mutation
}

/**
 * Probabilities of recombining
 */

void HapCopyHMM::SetTransition(void) {
  assert(gr->rho.size() == gr->pos.size() -1);
  r1.resize(gr->rho.size());
  // gr->rho has recombination map data
  size_t j;
  int d = 0;
  double ro, ndd(gr->m_N);
  for (j=0; j < gr->rho.size(); j++) {
    d = gr->pos[j+1] - gr->pos[j];
    assert(d >= 0);
    ro = gr->rho[j];
    r1[j] = (1-exp(-1*ro*d/ndd));
  }
}

void HapCopyHMM::Populate_t_post(const std::vector<size_t>& qidx,
                          const std::vector<size_t>& didx,
                          std::vector<std::vector<double> >* tpmx,
                          const std::vector<std::vector<double> >& prob) {
  assert(qidx.size() > 0 && didx.size() > 0);
  for (size_t t = 0; t < qidx.size(); t++) {
    for (size_t u = 0; u < didx.size(); u++) {
      (*tpmx)[t][u] = prob[t][u];
    }
  }
}

void HapCopyHMM::Forward() {
  // forward mx
  std::vector<std::vector<double> > ll(gr->m_N,
                                       std::vector<double>(gr->m_N));
  // initialize the first value to be the emmission probability
  for (size_t t=0; t < gr -> query_idx.size(); t++) {
    int gq = gr -> m_matrix[0][gr -> query_idx[t]];
    for (size_t u=0; u < gr -> donor_idx.size(); u++) {
      int gd = gr -> m_matrix[0][gr -> donor_idx[u]];
      ll[t][u] = (gq == gd)? e0 : e1;
    }
  }
  std::map<size_t, t_post>::iterator it;
  // if the first variant in target list
  // we shouldn't test for the first 1
  it = tmap.find(gr->pos[0]);
  if (it != tmap.end()) {
    tmap[gr->pos[0]].fw = ll;
  }

  for (size_t j=1; j < gr->m_M; j++) {
    bool keep = false;
    it = tmap.find(gr->pos[j]);
    if (it != tmap.end()) {
      keep = true;
    }
    double rhoj = r1[j-1];
    long double rhoN = rhoj/(gr->m_N-1);
    // loop over query sequences
    for (size_t t=0; t < gr->m_N; t++) {
      int gq = gr -> m_matrix[j][t];
      double sumj = 0;
      // loop donors for full calculation with emission
      for (size_t u=0; u < gr->m_N; u++) {
        if (t == u) {
          continue;  // do not copy from itself
        }
        int gd = gr -> m_matrix[j][u];
        double eps = (gq == gd)? e0 : e1;
        double valj = eps * ((1-rhoj)* ll[t][u] + rhoN);
        sumj += valj;
        ll[t][u] = valj;
      }
      // normalise by the sum
      for (size_t u=0; u < gr->m_N; u++) {
        ll[t][u] /= sumj;
      }
      if (keep) {
        Populate_t_post(tmap[gr->pos[j]].query_idx,
                        tmap[gr->pos[j]].donor_idx,
                        &tmap[gr->pos[j]].fw, ll);
      }
    }
    if (j%(M/10) == 0) {
      printf("Forward: Done position %lu\n", j+1);
    }
  }
}

void HapCopyHMM::Backward(void) {
  // initialize the first value to be the emmission probability
  std::vector<std::vector<double> > rr;  // local backward mx
  InitializeMatrix(&rr, gr->m_N, gr->m_N);

  double maxj, valj, ndd(gr->m_N);
  long double rhoj = 0.001, e, rmbsum(0);
  std::vector<double> eps(ndd, 0);
  // Initialize the last value to be 1
  for (size_t t=0; t < gr->m_N; t++) {
    for (size_t u=0; u < gr->m_N; u++) {
      rr[t][u] = 1;
    }
  }
  std::map<size_t, t_post>::iterator it;
  // if the first variant in target list
  // we shouldn't test for the first 1
  it = tmap.find(gr->pos[gr->m_M-1]);
  if (it != tmap.end()) {
    Populate_t_post(tmap[gr->pos[gr->m_M-1]].query_idx,
                    tmap[gr->pos[gr->m_M-1]].donor_idx,
                    &tmap[gr->pos[gr->m_M-1]].bw, rr);
  }

  int gq, gd;
  for (size_t j = M-2; j--;) {
    bool keep = false;
    it = tmap.find(gr->pos[j]);
    if (it != tmap.end()) {
      keep = true;
    }
    rhoj = r1[j];  // recomb rate at pos j
    // loop over query sequences
    for (size_t t=0; t < gr->m_N; t++) {
      eps.clear(), rmbsum = 0;
      gq = gr -> m_matrix[j+1][t];
      // loop donors and calculate shared sum
      for (size_t u=0; u < gr->m_N; u++) {
        if (t == u) {
          continue;  // do not copy from itself
        }
        gd = gr -> m_matrix[j+1][u];
        e = (gq == gd)? e0 : e1;
        eps[u] = e;
        rmbsum += rr[t][u] * e/ndd;
      }
      rmbsum *= rhoj;
      maxj = 0;
      // loop donors again for full calculation with emission
      for (size_t u=0; u < gr->m_N; u++) {
        if (t == u) {
          continue;  // do not copy from itself
        }
        valj = eps[u] * (1-rhoj) * rr[t][u] + rmbsum;
        if (valj > maxj) {
          maxj = valj;
        }
        rr[t][u] = valj;
      }
      // normalise by the max value
      for (size_t u=0; u < gr->m_N; u++) {
        rr[t][u] /= maxj;
      }
      if (keep) {
        Populate_t_post(tmap[gr->pos[j]].query_idx,
                        tmap[gr->pos[j]].donor_idx,
                        &tmap[gr->pos[j]].bw, rr);
      }
    }
    if (j%(M/10) == 0) {
      printf("Backward: Done position %lu\n", j+1);
    }
  }
}

void HapCopyHMM::WriteOutput(std::string outfilename,
                             std::string tag) {
  std::ofstream out;
  out.open(outfilename + tag, std::ofstream::out);
  std::map<size_t, t_post>::iterator it;
  for (it = tmap.begin(); it != tmap.end(); it++) {
    out << tag << "," << it->second.chrm << ",";
    out << it->second.pos << "," << it->second.ref << ",";
    out << it->second.alt << ",";
    out << std::setprecision(10) << it->second.l1 << ",";
    out << std::setprecision(10) << it->second.l2 << "\n";
  }
  out.close();
}

/**
* We compute likelihood for every position for each query hap
* just to double check results
*/
void HapCopyHMM::PositionLikelihood(void) {
  int nq = gr->query_idx.size();
  int nd = gr->donor_idx.size();
  InitializeMatrix(&llk, nq, M);
  int t, u;
  size_t j;
  double lhood, lf, rf, l, r;
  for (t=0; t < nq; t++) {
    for (j=0; j < M; j++) {
      lhood = 0.;
      l = 0.;
      r = 0.;
      for (u=0; u < nd; u++) lhood += left[t][u][j] * right[t][u][j];
      lf = VectorSum(&leftnorm[t], 0, j+1);
      rf = VectorSum(&rightnorm[t], j, M);
      llk[t][j] = log(lhood) + lf + rf;
    }
  }
}

void HapCopyHMM::SetQueries(void) {
  SetQueries(gr->pos[0], gr->pos[gr->m_M-1]);
}

void HapCopyHMM::SetQueries(size_t startpos, size_t endpos) {
  for (size_t i = 0; i < gr->pos.size(); i++) {
    if (gr->pos[i] < startpos || gr->pos[i] > endpos) {
      continue;
    }
    if (gr->minorAC[i] > gr->min_ac &&
        gr->minorAC[i] < gr->max_t_ac) {
      unsigned int ma = gr->majorA[i];
      t_post tp;
      tp.j_target = i;
      tp.chrm = gr->chrm[i];
      tp.ref = gr->ref[i];
      tp.alt = gr->alt1[i];
      tp.pos = gr->pos[i];
      for (size_t k = 0; k < gr->m_N; k++) {
        if (gr->m_matrix[i][k] == ma) {
          tp.donor_idx.push_back(i);
        } else {
          tp.query_idx.push_back(i);
        }
      }
      InitializeMatrix(&tp.fw,
                       tp.query_idx.size(),
                       tp.donor_idx.size());
      InitializeMatrix(&tp.bw,
                       tp.query_idx.size(),
                       tp.donor_idx.size());
      InitializeMatrix(&tp.pst,
                       tp.query_idx.size(),
                       tp.donor_idx.size());

      tmap.insert(std::pair<size_t, t_post>(tp.pos, tp));
    }
  }
}

void HapCopyHMM::ComputePosterior(void) {
  std::map<size_t, t_post>::iterator it;
  for (it = tmap.begin(); it != tmap.end(); it++) {
    InitializeMatrix(&it->second.pst,
                     it->second.query_idx.size(),
                     it->second.donor_idx.size());
    printf("Compute posterior for target site %lu\n", it->first);
    for (size_t t=0; t < it->second.query_idx.size(); t++) {
      double sum;
      sum = 0.;
      for (size_t u=0; u < it->second.donor_idx.size(); u++) {
        it->second.pst[t][u] = it->second.fw[t][u] * it->second.bw[t][u];
        sum += it->second.pst[t][u];
      }
      for (size_t u=0; u < it->second.donor_idx.size(); u++)
        it->second.pst[t][u] = it->second.pst[t][u]/= sum;
    }
    MutOriginTest(&it->second);
  }
  printf("Done posterior.\n");
}


void HapCopyHMM::MutOriginTest(t_post* tp) {
  double qk(1.), max1(-100000), max2(-100000);
  size_t u1, u2;
  std::map<size_t, t_post>::iterator it;

  for (size_t u=0; u < tp->donor_idx.size(); u++) {
    qk = 1.;
    for (size_t t=0; t < tp->query_idx.size(); t++) {
      qk += log(tp->pst[t][u]);
    }
    if (qk > max1) {
      max1 = qk;
      u1 = u;
    }
  }

  // copying from one haplotype
  for (size_t u=0; u < tp->donor_idx.size(); u++) {
    qk = 1.;
    for (size_t t=0; t < tp->query_idx.size(); t++) {
      qk += log(tp->pst[t][u]);
    }
    if (qk > max1) {
      max1 = qk;
      u1 = u;
    }
  }
  tp->l1 = max1;

  // copying from two haps, fix the first one
  for (size_t k = 0; k < tp->donor_idx.size(); k++) {
    if (k == u1) continue;
    qk = 1.;
    for (size_t t = 0; t < tp->query_idx.size(); t++) {
     qk += log(0.5*tp->pst[t][k] +
               0.5*tp->pst[t][u1]);
    }
    if (qk > max2) {
      max2 = qk;
      u2 = k;
    }
  }
  tp->l2 = max2;
}

void HapCopyHMM::Debug(std::string outpath,
                       unsigned int j_target,
                       std::string tag) {
  int nq = gr->query_idx.size();
  int nd = gr->donor_idx.size();
  std::ofstream leftfile, rightfile, postfile, llkfile, post_j_file, ham_j_file;
  leftfile.open(outpath + ".debug-left-hap0" + tag, std::ofstream::out);
  rightfile.open(outpath + ".debug-right-hap0" + tag, std::ofstream::out);
  postfile.open(outpath + ".debug-post-hap0" + tag, std::ofstream::out);
  post_j_file.open(outpath + ".post-j-" +
                   std::to_string(gr->pos[j_target]) + tag,
                   std::ofstream::out);
  ham_j_file.open(outpath + ".hamming-j-" +
                   std::to_string(gr->pos[j_target]) + tag,
                   std::ofstream::out);
  // posterior and hamming distance
  gr->HammingDistance();
  int t, u;
  for (t = 0; t < nq; t++) {
    post_j_file << gr->query_idx[t] << std::scientific;
    ham_j_file <<  gr->query_idx[t] << std::scientific;
    for (u = 0; u < nd; u++) {
      // post_j_file << "," <<posterior[t][u];
      ham_j_file << "," << gr -> hamd[t][u];
    }
    post_j_file << "\n";
    ham_j_file << "\n";
  }

  post_j_file.close();
  ham_j_file.close();

  // debug, just write out stuff for the first hap
  t = 0;
  for (int d = 0; d < nd; d++) {
    leftfile << d << std::scientific;
    rightfile << d << std::scientific;
    postfile << d << std::scientific;
    post_j_file << d << std::scientific;
    for (unsigned int j = 0; j < M; j++) {
      leftfile << "," << left[t][d][j];
      rightfile << "," << right[t][d][j];
    }
    leftfile <<  "\n";
    rightfile << "\n";
  }
  leftfile.close();
  rightfile.close();

  std::ofstream leftnormfile;
  leftnormfile.open(outpath + ".debug-leftnorm-hap0" + tag,
                    std::ofstream::out);
  std::ofstream rightnormfile;
  rightnormfile.open(outpath + ".debug-rightnorm-hap0" + tag,
                     std::ofstream::out);

  leftnormfile << t << std::scientific;
  rightnormfile << t << std::scientific;
  for (unsigned int j = 0; j < M; j++) {
    leftnormfile << "," << leftnorm[t][j];
    rightnormfile << "," << rightnorm[t][j];
  }
  leftnormfile << "\n";
  rightnormfile << "\n";
  leftnormfile.close();
  rightnormfile.close();

  llkfile.open(outpath + ".debug-llk-allhap-allpos" + tag, std::ofstream::out);
  llkfile << std::scientific;
  for (int t = 0; t < nq; t++) {
    for (unsigned int j = 0; j < M; j++) {
      llkfile << "," << llk[t][j];
    }
    llkfile << "\n";
  }
  llkfile.close();
}


}  // namespace UTSNP
